# WeAreMarcus
WeAreMarcus is a mentoring, character development video streaming application which groups series into chapters and episodes. Curriculum responses are recorded at the end of every chapter video.

Cloudinary.com is used for the video storage and streaming.

Electron.io framework supports native library integration on desktops and also allowing for a platform agnostic deployment across classrooms with an olio of machines, browsers, and versions of the former.

#### login screen
![login screen](docs/1.png "login screen")
#### side menu
![side menu](docs/3.png "side menu")
#### episodes
![episodes](docs/2.png "episodes")
#### chapters
![chapters](docs/4.png "chapters")
#### player
![player](docs/5.png "player")
#### response
![response](docs/6.png "response")


## serving the app locally
This will launch the app and reload on html changes

    gulp serve


### Build
This command packages the application for x64 windows machines
````

electron-packager . we-are-marcus --platform=win32 --arch=x64 --overwrite

````

### Test the build

This command serves the minified version of the app in an unbundled state, as it would
be served by a push-compatible server:

    polymer serve build/unbundled

This command serves the minified version of the app generated using fragment bundling:

    polymer serve build/bundled
